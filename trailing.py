def maxTrailing(b, d, a):
    maxMultiplier = a // b
    maxcount = 0
    while (maxMultiplier):
        result = b * maxMultiplier
        if str(d) in str(result):
            count = (trailing(d, result))
            if(maxcount < count):
                maxcount = count;
        maxMultiplier-= 1;
    return maxcount
def trailing(d,result):
    count = 0  
    for i in str(result):
        if i == str(d):
            count +=1
        else: 
            count = 0
    return count if count >=2 else 0
p,q,r=map(int,input().split(" ")) 
while p*q*r !=0:
    print(maxTrailing(p,q,r))
    p,q,r=map(int,input().split(" "))
